const axios = require('axios')
const xml2js = require('xml2js-es6-promise')

async function processError (e) {
  // xml parsing options
  const opts = {
    explicitArray: false,
    trim: true
  }
  try {
    const error = await xml2js(e.response.data, opts)
    return error
  } catch (e2) {
    return e
  }
}

module.exports = class Spaces {
  constructor({ url, username, password }) {
    this.url = `${url}/ldapSyncs`
    this.auth = {
      username,
      password
    }
  }

  async list(params) {
    // list ldap syncs in progress
    let response
    try {
      response = await axios.get(this.url, {params, auth: this.auth})
    } catch (e) {
      throw e
    }

    // parse xml response to json
    try {
      // xml parsing options
      const opts = {
        explicitArray: false,
        trim: true
      }
      // parse xml to json and return that json data as an array
      return await xml2js(response.data, opts)
    } catch (e) {
      console.log('error parsing XML to JSON:', e)
      throw e
    }
  }

  async sync () {
    // start LDAP sync
    let data = {}
    try {
      return await axios.post(this.url, data, {auth: this.auth})
    } catch (e) {
      throw processError(e)
    }
  }
}
