# cms-api-client Change Log

Version numbers are semver-compatible dates in YYYY.MM.DD-X format,
where X is the revision number

# 2020.10.26

### Features
* **LDAP Sync:** Add methods to start LDAP sync and get list of current LDAP
sync operations.
